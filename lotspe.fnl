(local CONF {:_AUTHOR "parlortricks"
             :_EMAIL "palortricks@fastmail.fm"
             :_NAME "LOTSPE"
             :_VERSION "1.0"
             :_URL "https://gitlab.com/build-engine/lotspe"
             :_DESCRIPTION "Legend of the Seven Paladins Extracter"
             :_LICENSE "    MIT LICENSE
    Copyright 2022 parlortricks <parlortricks@fastmail.fm>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the \"Software\"), to 
    deal in the Software without restriction, including without limitation the 
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
    sell copies of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in 
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE."})

(set CONF._HELP (..
"Legend of the Seven Paladins Extracter v" CONF._VERSION "\n" 
"===========================================\n"
"Copyright 2022 " CONF._AUTHOR " <" CONF._EMAIL ">\n\n"
"Usage: fennel lotspe.fnl [file to extract]
  
  Works on the following files:
   - LMART000.DAT
   - LVART000.DAT

  Options:
  -h, --help      Display this help
  -v, --version   Display the version   
   
Based off works from Frenkel https://sfprod.shikadi.net/games/lotsp.htm"))	             

(fn save-files-to-disk [data]
  (each [i file (ipairs data)]
    (let [filename (.. i "." file.type)]
      (with-open [fout (io.open filename :wb)]
        (fout:write file.data)))))

(fn calculate-offset [file-size data]
  (each [i file (ipairs data)]
    (if (< i (length data))
      (tset data i :size (- (. (. data (+ i 1)) :offset) (. (. data i) :offset)))  
      (tset data i :size (- file-size (. (. data i) :offset))))))

(fn determine-file-type [fin data]
  (each [_ file (ipairs data)]
    (fin:seek :set file.offset) 
    (set file.data (fin:read file.size))
    (if (or (= (file.data:byte 1) 0) (= (file.data:byte 1) 31))
          (set file.type :pal)
        (= (file.data:byte 1) 4)
          (set file.type :map)          
        (= (file.data:byte 1) 10)
          (set file.type :pcx)
        (= (file.data:byte 1) 67)
          (set file.type :voc)
        (= (file.data:byte 1) 77)
            (set file.type :mid))))      

(fn extract [file]  
  (let [files []]
    (with-open [fin (io.open file :rb)]
      (var file-size (fin:seek :end))
      (fin:seek :set 0)

      (var read-offsets true)
      (while read-offsets
        (let [file-offset (string.unpack "<I4" (fin:read 4))]
          (if (not= file-offset file-size)
            (do
              (local file {:offset file-offset})
              (table.insert files file) )
            (do
              (set read-offsets false)))))

      (calculate-offset file-size files)
      (determine-file-type fin files))           
    (save-files-to-disk files)))

(if (or (= (length arg) 0) (: (. arg 1) :match "-h") (: (. arg 1) :match "--help"))
  (do
    (print CONF._HELP))
  (do
    (if (or (: (. arg 1) :match "ARCHIVE.DAT") (: (. arg 1) :match "LMART000.DAT") (: (. arg 1) :match "LVART000.DAT"))
          (extract (. arg 1))
        (or (: (. arg 1) :match "-v") (: (. arg 1) :match "--version"))
          (print (.. CONF._NAME " v" CONF._VERSION))
        (do
          (print "Can only extract LMART000.DAT or LVART000.DAT")))))